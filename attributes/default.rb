
default['util-consul']['diplomat']['version']    = '2.0.2'
default['util-consul']['consul']['url']          = 'http://localhost:8500'
default['util-consul']['consul']['acl_token']    = nil
