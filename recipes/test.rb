#
# Cookbook Name:: util-consul
# Recipe:: test
#

# if Chef::Config[:file_cache_path].include?('kitchen')
chef_gem 'diplomat' do
  version node['util-consul']['diplomat']['version']
  action :install
end

Diplomat.configure do |config|
  config.url = node['util-consul']['consul']['url']
  config.acl_token = node['util-consul']['consul']['acl_token']
  config.options = { request: { timeout: 10 } }
end

#  directory node['util-consul']['test']['consul']['install_dir'] do
#    recursive true
#  end
#
#  directory node['util-consul']['test']['consul']['config_dir'] do
#    recursive true
#  end
#
#  directory node['util-consul']['test']['consul']['data_dir'] do
#    recursive true
#  end
#
#  remote_file "#{node['util-consul']['test']['consul']['install_dir']}/consul.zip" do
#    source "https://releases.hashicorp.com/consul/#{node['util-consul']['test']['consul']['version']}/consul_#{node['util-consul']['test']['consul']['version']}_linux_amd64.zip"
#    mode '0755'
#  end
#
#  package 'unzip'
#
#  execute 'extract_consul_archive' do
#    command "unzip -f #{node['util-consul']['test']['consul']['install_dir']}/consul.zip"
#    cwd node['util-consul']['test']['consul']['install_dir']
#    only_if { ::File.exist?("#{node['util-consul']['test']['consul']['install_dir']}/consul.zip") }
#  end
#
#  file "#{node['util-consul']['test']['consul']['install_dir']}/consul.zip" do
#    action :delete
#  end
#
#  execute 'enable consul service' do
#    command "#{node['util-consul']['test']['consul']['install_dir']}/consul agent -server -client=0.0.0.0 -bootstrap-expect=1 -ui -dev &"
#    not_if 'ps -aef|grep consul|grep -v grep'
#  end

consul_key 'test' do
  value 'test'
end

consul_key 'test' do
  action :destroy
end

acl_value = {
  Name: 'test',
  Type: 'client'
}

consul_acl 'test' do
  value acl_value
end
