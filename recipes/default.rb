#
# Cookbook Name:: util-consul
# Recipe:: default
#

chef_gem 'diplomat' do
  version node['util-consul']['diplomat']['version']
  action :install
end

Diplomat.configure do |config|
  config.url = node['util-consul']['consul']['url']
  config.acl_token = node['util-consul']['consul']['acl_token']
  config.options = { request: { timeout: 10 } }
end
