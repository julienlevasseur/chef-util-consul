class Chef
  class Recipe
    module Consul
      require 'diplomat'
      require 'chef-helpers'

      module Key
        def self.exist?(key)
          return true if ::Diplomat::Kv.get(key, recurse: false)
        rescue ::Diplomat::KeyNotFound
          return false
        end

        def self.get(key)
          ::Diplomat::Kv.get(key, recurse: false)
        end

        def self.put(key, value)
          ::Diplomat::Kv.put(key, value)
        end

        def self.destroy(key)
          ::Diplomat::Kv.delete(key)
        end
      end

      module Service
        def self.get_first(name)
          ::Diplomat::Service.get(name)
        end

        def self.get_all(name)
          ::Diplomat::Service.get(name, :all)
        end
      end

      module Acl
        def self.exist?(id)
          return true if ::Diplomat::Acl.info(id)
        rescue ::Diplomat::KeyNotFound
          return false
        end

        def self.info(id)
          ::Diplomat::Acl.info(id)
        end

        def self.list
          ::Diplomat::Acl.list()
        end

        def self.update(value) # Create if not present
          ::Diplomat::Acl.update(value)
        end

        def self.destroy(id)
          ::Diplomat::Acl.destroy(id)
        end
      end
    end
  end
end
