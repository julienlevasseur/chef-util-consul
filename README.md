# util-consul

This cookbook provide helpers to communicate with Consul

## Requirements

This cookbook has no requirement

## Attributes

* `node['util-consul']['diplomat']['version']` - The Diplomatgem version to install - Default: '1.2.0'
* `node['util-consul']['consul']['url']` - The Consul cluster url - Default: 'http://consul-01.velolibreservice.ca:8500'
* `node['util-consul']['consul']['acl_token']` - ACL Token to authenticate Chef to Consul - Default: nil

## Recipes

### default

Manage Diplomat gem installation and configuration

## Usage

`metadata.rb` :
```ruby
depends 'util-consul'
```

`cookbook::recipe` :
```ruby
attributes.each do |attribute|
  if ConsulKey.exist?("chef/#{attribute}")
    node.force_override[attribute] = "#{ConsulKey.get("chef/#{attribute}")}"
  end
end
```

## References

* https://github.com/WeAreFarmGeek/diplomat
* https://www.consul.io/docs/index.html
